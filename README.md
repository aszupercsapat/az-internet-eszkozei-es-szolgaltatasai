# Az Internet eszközei és szolgáltatásai

## Vizsga 2019.04.29!
- 40 kérdés
- Teszt kérdések
- 1-2 soros válaszok (kifejtős nem)
- http protokol a legtöbb
- minden témából, egyszerább kérdések (pl felsorolások)
- szemantikus web (RDF célja, RDF hármas komponensei, felrajzolni egy RDF kiejlentést, stb...)
- URI-król feloldás, stb...
- küld tájékoztatást
- 2 nap alatt kijavítja

## Szóbeli
- mint a progtech
- >= 90% 2 pont
- >= 50% 1 pont

## Szakmai napos felajánlás
- max 5%
- még kitalálja, hogy-hogy, megírja Neptunon

## Előadások - vége

1. előadás  
[World Wide Web - Végig](https://arato.inf.unideb.hu/jeszenszky.peter/download/ie/presentations/www-intro.pdf)  
[IANA média típusok - Néhány főbb média típus](https://arato.inf.unideb.hu/jeszenszky.peter/download/ie/presentations/media-types.pdf)  
[Egységes erőforrás-azonosító (URI)](https://arato.inf.unideb.hu/jeszenszky.peter/download/ie/presentations/uri.pdf)  

2. előadás  
[Egységes erőforrás-azonosító (URI) - Végig](https://arato.inf.unideb.hu/jeszenszky.peter/download/ie/presentations/uri.pdf)  
[Hypertext Transfer Protocol (HTTP) - Payload szemantika](https://arato.inf.unideb.hu/jeszenszky.peter/download/ie/presentations/http.pdf)  

3. előadás  
[Hypertext Transfer Protocol (HTTP) - ...](https://arato.inf.unideb.hu/jeszenszky.peter/download/ie/presentations/http.pdf)

4. előadás  
[Hypertext Transfer Protocol (HTTP) - HTTP/1.0 kapcsolatkezelés](https://arato.inf.unideb.hu/jeszenszky.peter/download/ie/presentations/http.pdf)

5. előadás  
[Hypertext Transfer Protocol (HTTP) - Végig](https://arato.inf.unideb.hu/jeszenszky.peter/download/ie/presentations/http.pdf)  
[HTTP/2 (Még nincs fólia 19.03.11)]()  

- Bevált gyakolratok a késleltetési idő csökkentésére
- Mi a HTTP/2?
- Fejlesztés
- Specifikációk
- A HTTP/2 újdonságai
- Üzenet multiplexelés
- Elterjedtség
- Implementációk
- Hivatalos Java támogatás
- Böngésző kiegészítők
- curl
- Fogalmak: Kliens, Szerver, Végpont, Kapcsolat, Keret, Adatfolyam
- HTTP/2 kapcsolat létrehozása
- ALPN
- Keretek felépítése
- Keretek mérete
- Keretek fajtái
- Adatfolyamok jellemzői
- Adatfolyamok azonosítása
- HTTP kérés/válasz váltás
- HTTP/2 üzenetek felépítése
- Fejlécmezők
- Pszeudo-fejlécmezők
- Szerver push
- Kapcsolatkezelés
- HPACK
- Fogalmak: Fejlécmező, Fejléc lista, Fejléc blokk, Fejléc blokk töredék
- Indextáblák
- Fejlécmezők ábrázolása
- Huffman-kódolás
[XML]()  

- Mi az XML?
- Előzmény
- Jelölőnyelvek
- ...

6. előadás  
...

7. előadás  
...

8. előadás  
[Semantic Web (Még nincs 19.04.01)]()  

- Definíció, Vízió, Az alapul szolgáló mechanizmusok szabványok
- RDF
- Specifikációk
- Alapfogalmak (Erőforrás, Literál, Adattípus, Üres csomópont, RDF term...)
- Erőforrások
- Literálok
- Adattípusok
- Üres csomópont
- RDF termek
- RDF hármas
- RDF szótárak
- RDF gráfok
- RDF dokumentumok
- RDF konkrét szintaxisai
- RDF gráfok vizuális ábrázolása
- SPARQL
- SPARQL lekérdezések
- Gráfminták
- Részgráfillesztési feladat
- SPARQL implementációk
[Linked Data (Még nincs 19.04.01)]()  

- Linked Data
- Specifikációk
- Felhasznált fogalmak (URI, IRI, Erőforrásrész-azonosító,...)
- RDF linkek
- Böngészés
- Böngészhető gráfok
- Linked Data vs Web APIs
- Alapelvek
- URI-k használata
- Hash URI-k
- 303-as átirányítás
- Kapcsolt adatok közzététele
- További eszközök és információk fejlesztők számára
- Nyílt adatok
- Linked Open Data
- GeoNames
- DBpedia
- DBpedia: tartalom
- DBpedia: URI-k
- DBpedia: online elérés
- DBpedia: SPARQL lekérdezések
- DBpedia elérés Java-ból
- DBpedia: felhasználási lehetőségek
- DBpedia: alkalmazások

9. előadás
[XML Schema (még nincs 2019.04.08)]()

- XML sémanyelvek
- RELAX NG, Schematron
- A DTD korlátai
- W3C XML Schema
- W3C XML Schema Definition Language
- Jellemzők
- Típushieracrhia
- Adattípusok fajtái
- Adattípus fogalma (Értéktér, Lexikális tér, adattípus-tulajdonságok)
- Beépített adattípusok
[Reszponzív webdesign (nincs 2019.04.08)]()

- Webelérés asztali és mobil eszközökről
- Mobilegeddon
- Mobile-First indexing
- Mobil tartalomszolgáltatás
- Mi a reszponzív webdesign?
- Mi az adaptív webdesign?
- Weboldal elrendezések
- Reszponzív webhelyek
- Mi egy pixel?
- A reszponzív webdesign alkotóelemei
- CSS nézetablak mértékegységek
- Mik a média lekérdezések?
- Média lekérdezés specifikáció
- Média lekérdezés támogatás
- Média lekérdezések rendelkezésre állása
- Média lekérdezések CSS-ben
- Média lekérdezések XML-ben
- Média lekérdezések HTML-ben
- Média lekérdezések JavaScript-ben
- Mértékegységek média lekérdezésekben
- Média lekérdezés szintaxis
- Média lekérdezés kiértékelés
- Média lekérdezés módosítók
- Média típusok
- Média jellemzők
- Média jellemzők kiértékelése tartomány kontextusban
- Média jellemzők kombinálása
- Rendelkezésre álló média jellemzők
- Tipikus média lekérdezés töréspontok
- @viewport
- Folyékony rács
- Flexbox elrendezés
- Rács elrendezés
- Rugalmas helyettesített elemek
- Adatptív/reszponzív képek

## Vizsgán lesz (nem teljes lista)

A vizsga az [előadás anyagából](https://arato.inf.unideb.hu/jeszenszky.peter/download/ie/presentations/) lesz  
"az írásbeli vizsga utolsó előadás időpontjában lesz, és jegymegajánló formájában. nem számít vizsgaalkalomnak."  

-	URI szintaxis (2)
-	Példák relatív hivatkozások feloldására (1)
-	http 80 / https 443
-	A http és https URI-séma (3) – ekvivalensek?

